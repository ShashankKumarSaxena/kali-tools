---
Title: socat
Homepage: https://www.dest-unreach.org/socat/
Repository: 
Architectures: any
Version: 1.8.0.0-4
Metapackages: kali-linux-default kali-linux-everything kali-linux-headless kali-linux-labs kali-linux-large kali-linux-nethunter kali-tools-web 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### socat
 
  Socat (for SOcket CAT) establishes two bidirectional byte streams
  and transfers data between them. Data channels may be files, pipes,
  devices (terminal or modem, etc.), or sockets (Unix, IPv4, IPv6, raw,
  UDP, TCP, SSL). It provides forking, logging and tracing, different
  modes for interprocess communication and many more options.
   
  It can be used, for example, as a TCP relay (one-shot or daemon),
  as an external socksifier, as a shell interface to Unix sockets,
  as an IPv6 relay, as a netcat and rinetd replacement, to redirect
  TCP-oriented programs to a serial line, or to establish a relatively
  secure environment (su and chroot) for running client or server shell
  scripts inside network connections. Socat supports sctp as of 1.7.0.
 
 **Installed size:** `1.71 MB`  
 **How to install:** `sudo apt install socat`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 * libssl3 
 * libwrap0 
 {{< /spoiler >}}
 
 ##### filan
 
 Multipurpose relay (SOcket CAT)
 
 ```
 root@kali:~# filan -h
 filan by Gerhard Rieger and contributors - see http://www.dest-unreach.org/socat/
 Analyze file descriptors of the process
 Usage:
 filan [options]
    options:
       -?|-h          print this help text
       -d             increase verbosity (use up to 4 times)
       -i<fdnum>      only analyze this fd
       -n<fdnum>      analyze all fds from 0 up to fdnum-1 (default: 1024)
       -s             simple output with just type and socket address or path
       -S             like -s but improved format and contents
       -f<filename>   analyze file system entry
       -T<seconds>    wait before analyzing, useful to connect with debugger
       -r             raw output for time stamps and rdev
       -L             follow symbolic links instead of showing their properties
       -o<filename>   output goes to filename, that can be:
                      a regular file name, the output goes to that
                      +<filedes> , output goes to the file descriptor (which must be open writable)
                      the 3 special names stdin stdout and stderr
 ```
 
 - - -
 
 ##### procan
 
 Multipurpose relay (SOcket CAT)
 
 ```
 root@kali:~# procan -h
 procan by Gerhard Rieger and contributors - send bug reports to socat@dest-unreach.org
 Analyze system parameters of process
 Usage:
 procan [options]
    options:
       -?|-h  print a help text describing command line options
       -c     print values of compile time C defines
 ```
 
 - - -
 
 ##### socat
 
 Multipurpose relay (SOcket CAT)
 
 ```
 root@kali:~# socat -h
 socat by Gerhard Rieger and contributors - see www.dest-unreach.org
 Usage:
 socat [options] <bi-address> <bi-address>
    options (general command line options):
       -V     print version and feature information to stdout, and exit
       -h|-?  print a help text describing command line options and addresses
       -hh    like -h, plus a list of all common address option names
       -hhh   like -hh, plus a list of all available address option names
       -d[ddd]        increase verbosity (use up to 4 times; 2 are recommended)
       -d0|1|2|3|4    set verbosity level (0: Errors; 4 all including Debug)
       -D     analyze file descriptors before loop
       --experimental enable experimental features
       --statistics   output transfer statistics on exit
       -ly[facility]  log to syslog, using facility (default is daemon)
       -lf<logfile>   log to file
       -ls            log to stderr (default if no other log)
       -lm[facility]  mixed log mode (stderr during initialization, then syslog)
       -lp<progname>  set the program name used for logging and vars
       -lu            use microseconds for logging timestamps
       -lh            add hostname to log messages
       -v     verbose text dump of data traffic
       -x     verbose hexadecimal dump of data traffic
       -r <file>      raw dump of data flowing from left to right
       -R <file>      raw dump of data flowing from right to left
       -b<size_t>     set data buffer size (8192)
       -s     sloppy (continue on error)
       -S<sigmask>    log these signals, override default
       -t<timeout>    wait seconds before closing second channel
       -T<timeout>    total inactivity timeout in seconds
       -u     unidirectional mode (left to right)
       -U     unidirectional mode (right to left)
       -g     do not check option groups
       -L <lockfile>  try to obtain lock, or fail
       -W <lockfile>  try to obtain lock, or wait
       -4     prefer IPv4 if version is not explicitly specified
       -6     prefer IPv6 if version is not explicitly specified
    bi-address:  /* is an address that may act both as data sync and source */
       <single-address>
       <single-address>!!<single-address>
    single-address:
       <address-head>[,<opts>]
    address-head:
       ABSTRACT-CLIENT:<filename>		groups=FD,SOCKET,RETRY,UNIX
       ABSTRACT-CONNECT:<filename>		groups=FD,SOCKET,RETRY,UNIX
       ABSTRACT-LISTEN:<filename>		groups=FD,SOCKET,LISTEN,CHILD,RETRY,UNIX
       ABSTRACT-RECV:<filename>			groups=FD,SOCKET,RETRY,UNIX
       ABSTRACT-RECVFROM:<filename>		groups=FD,SOCKET,CHILD,RETRY,UNIX
       ABSTRACT-SENDTO:<filename>		groups=FD,SOCKET,RETRY,UNIX
       ACCEPT-FD:<fdnum>				groups=FD,SOCKET,CHILD,RETRY,RANGE,UNIX,IP4,IP6,UDP,TCP
       CREATE:<filename>				groups=FD,REG,NAMED
       DCCP-CONNECT:<host>:<port>		groups=FD,SOCKET,CHILD,RETRY,IP4,IP6
       DCCP-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RETRY,RANGE,IP4,IP6
       DCCP4-CONNECT:<host>:<port>		groups=FD,SOCKET,CHILD,RETRY,IP4
       DCCP4-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RETRY,RANGE,IP4
       DCCP6-CONNECT:<host>:<port>		groups=FD,SOCKET,CHILD,RETRY,IP6
       DCCP6-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RETRY,RANGE,IP6
       EXEC:<command-line>			groups=FD,FIFO,SOCKET,EXEC,FORK,TERMIOS,PTY,PARENT,UNIX
       FD:<fdnum>				groups=FD,FIFO,CHR,BLK,REG,SOCKET,TERMIOS,UNIX,IP4,IP6,UDP,TCP
       GOPEN:<filename>				groups=FD,FIFO,CHR,BLK,REG,SOCKET,NAMED,OPEN,TERMIOS,UNIX
       INTERFACE:<interface>			groups=FD,SOCKET,INTERFACE
       IP-DATAGRAM:<host>:<protocol>		groups=FD,SOCKET,RANGE,IP4,IP6
       IP-RECV:<protocol>			groups=FD,SOCKET,RANGE,IP4,IP6
       IP-RECVFROM:<protocol>			groups=FD,SOCKET,CHILD,RANGE,IP4,IP6
       IP-SENDTO:<host>:<protocol>		groups=FD,SOCKET,IP4,IP6
       IP4-DATAGRAM:<host>:<protocol>		groups=FD,SOCKET,RANGE,IP4
       IP4-RECV:<protocol>			groups=FD,SOCKET,RANGE,IP4
       IP4-RECVFROM:<protocol>			groups=FD,SOCKET,CHILD,RANGE,IP4
       IP4-SENDTO:<host>:<protocol>		groups=FD,SOCKET,IP4
       IP6-DATAGRAM:<host>:<protocol>		groups=FD,SOCKET,RANGE,IP6
       IP6-RECV:<protocol>			groups=FD,SOCKET,RANGE,IP6
       IP6-RECVFROM:<protocol>			groups=FD,SOCKET,CHILD,RANGE,IP6
       IP6-SENDTO:<host>:<protocol>		groups=FD,SOCKET,IP6
       OPEN:<filename>				groups=FD,FIFO,CHR,BLK,REG,NAMED,OPEN,TERMIOS
       OPENSSL:<host>:<port>			groups=FD,SOCKET,CHILD,RETRY,IP4,IP6,TCP,OPENSSL
       OPENSSL-DTLS-CLIENT:<host>:<port>		groups=FD,SOCKET,CHILD,RETRY,IP4,IP6,UDP,OPENSSL
       OPENSSL-DTLS-SERVER:<port>		groups=FD,SOCKET,LISTEN,CHILD,RETRY,RANGE,IP4,IP6,UDP,OPENSSL
       OPENSSL-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RETRY,RANGE,IP4,IP6,TCP,OPENSSL
       PIPE[:<filename>]				groups=FD,FIFO,NAMED,OPEN
       POSIXMQ-BIDIRECTIONAL:<mqname>		groups=FD,NAMED,RETRY
       POSIXMQ-READ:<mqname>			groups=FD,NAMED,RETRY
       POSIXMQ-RECEIVE:<mqname>			groups=FD,NAMED,CHILD,RETRY
       POSIXMQ-SEND:<mqname>			groups=FD,NAMED,CHILD,RETRY
       PROXY:<proxy-server>:<host>:<port>	groups=FD,SOCKET,CHILD,RETRY,IP4,IP6,TCP,HTTP
       PTY					groups=FD,NAMED,TERMIOS,PTY
       SCTP-CONNECT:<host>:<port>		groups=FD,SOCKET,CHILD,RETRY,IP4,IP6
       SCTP-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RETRY,RANGE,IP4,IP6
       SCTP4-CONNECT:<host>:<port>		groups=FD,SOCKET,CHILD,RETRY,IP4
       SCTP4-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RETRY,RANGE,IP4
       SCTP6-CONNECT:<host>:<port>		groups=FD,SOCKET,CHILD,RETRY,IP6
       SCTP6-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RETRY,RANGE,IP6
       SHELL:<shell-command>			groups=FD,FIFO,SOCKET,EXEC,FORK,SHELL,TERMIOS,PTY,PARENT,UNIX
       SOCKET-CONNECT:<domain>:<protocol>:<remote-address>	groups=FD,SOCKET,CHILD,RETRY
       SOCKET-DATAGRAM:<domain>:<type>:<protocol>:<remote-address>	groups=FD,SOCKET,RANGE
       SOCKET-LISTEN:<domain>:<protocol>:<local-address>	groups=FD,SOCKET,LISTEN,CHILD,RETRY,RANGE
       SOCKET-RECV:<domain>:<type>:<protocol>:<local-address>	groups=FD,SOCKET,RANGE
       SOCKET-RECVFROM:<domain>:<type>:<protocol>:<local-address>	groups=FD,SOCKET,CHILD,RANGE
       SOCKET-SENDTO:<domain>:<type>:<protocol>:<remote-address>	groups=FD,SOCKET
       SOCKETPAIR:<filename>			groups=FD,SOCKET
       SOCKS4:<socks-server>:<host>:<port>	groups=FD,SOCKET,CHILD,RETRY,IP4,IP6,TCP,SOCKS4
       SOCKS4A:<socks-server>:<host>:<port>	groups=FD,SOCKET,CHILD,RETRY,IP4,IP6,TCP,SOCKS4
       SOCKS5-CONNECT:<socks-server>:<socks-port>:<target-host>:<target-port>	groups=FD,SOCKET,CHILD,RETRY,IP4,IP6,TCP
       SOCKS5-LISTEN:<socks-server>:<socks-port>:<listen-host>:<listen-port>	groups=FD,SOCKET,CHILD,RETRY,IP4,IP6,TCP
       STDERR					groups=FD,FIFO,CHR,BLK,REG,SOCKET,TERMIOS,UNIX,IP4,IP6,UDP,TCP
       STDIN					groups=FD,FIFO,CHR,BLK,REG,SOCKET,TERMIOS,UNIX,IP4,IP6,UDP,TCP
       STDIO					groups=FD,FIFO,CHR,BLK,REG,SOCKET,TERMIOS,UNIX,IP4,IP6,UDP,TCP
       STDOUT					groups=FD,FIFO,CHR,BLK,REG,SOCKET,TERMIOS,UNIX,IP4,IP6,UDP,TCP
       SYSTEM:<shell-command>			groups=FD,FIFO,SOCKET,EXEC,FORK,TERMIOS,PTY,PARENT,UNIX
       TCP-CONNECT:<host>:<port>			groups=FD,SOCKET,CHILD,RETRY,IP4,IP6,TCP
       TCP-LISTEN:<port>				groups=FD,SOCKET,LISTEN,CHILD,RETRY,RANGE,IP4,IP6,TCP
       TCP4-CONNECT:<host>:<port>		groups=FD,SOCKET,CHILD,RETRY,IP4,TCP
       TCP4-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RETRY,RANGE,IP4,TCP
       TCP6-CONNECT:<host>:<port>		groups=FD,SOCKET,CHILD,RETRY,IP6,TCP
       TCP6-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RETRY,RANGE,IP6,TCP
       TUN[:<ip-addr>/<bits>]			groups=FD,CHR,OPEN,INTERFACE
       UDP-CONNECT:<host>:<port>			groups=FD,SOCKET,IP4,IP6,UDP
       UDP-DATAGRAM:<host>:<port>		groups=FD,SOCKET,RANGE,IP4,IP6,UDP
       UDP-LISTEN:<port>				groups=FD,SOCKET,LISTEN,CHILD,RANGE,IP4,IP6,UDP
       UDP-RECV:<port>				groups=FD,SOCKET,RANGE,IP4,IP6,UDP
       UDP-RECVFROM:<port>			groups=FD,SOCKET,CHILD,RANGE,IP4,IP6,UDP
       UDP-SENDTO:<host>:<port>			groups=FD,SOCKET,IP4,IP6,UDP
       UDP4-CONNECT:<host>:<port>		groups=FD,SOCKET,IP4,UDP
       UDP4-DATAGRAM:<host>:<port>		groups=FD,SOCKET,RANGE,IP4,UDP
       UDP4-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RANGE,IP4,UDP
       UDP4-RECV:<port>				groups=FD,SOCKET,RANGE,IP4,UDP
       UDP4-RECVFROM:<port>			groups=FD,SOCKET,CHILD,RANGE,IP4,UDP
       UDP4-SENDTO:<host>:<port>			groups=FD,SOCKET,IP4,UDP
       UDP6-CONNECT:<host>:<port>		groups=FD,SOCKET,IP6,UDP
       UDP6-DATAGRAM:<host>:<port>		groups=FD,SOCKET,RANGE,IP6,UDP
       UDP6-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RANGE,IP6,UDP
       UDP6-RECV:<port>				groups=FD,SOCKET,RANGE,IP6,UDP
       UDP6-RECVFROM:<port>			groups=FD,SOCKET,CHILD,RANGE,IP6,UDP
       UDP6-SENDTO:<host>:<port>			groups=FD,SOCKET,IP6,UDP
       UDPLITE-CONNECT:<host>:<port>		groups=FD,SOCKET,IP4,IP6,UDP
       UDPLITE-DATAGRAM:<host>:<port>		groups=FD,SOCKET,RANGE,IP4,IP6,UDP
       UDPLITE-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RANGE,IP4,IP6,UDP
       UDPLITE-RECV:<port>			groups=FD,SOCKET,RANGE,IP4,IP6,UDP
       UDPLITE-RECVFROM:<port>			groups=FD,SOCKET,CHILD,RANGE,IP4,IP6,UDP
       UDPLITE-SENDTO:<host>:<port>		groups=FD,SOCKET,IP4,IP6,UDP
       UDPLITE4-CONNECT:<host>:<port>		groups=FD,SOCKET,IP4,UDP
       UDPLITE4-DATAGRAM:<remote-address>:<port>	groups=FD,SOCKET,RANGE,IP4,UDP
       UDPLITE4-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RANGE,IP4,UDP
       UDPLITE4-RECV:<port>			groups=FD,SOCKET,RANGE,IP4,UDP
       UDPLITE4-RECVFROM:<host>:<port>		groups=FD,SOCKET,CHILD,RANGE,IP4,UDP
       UDPLITE4-SENDTO:<host>:<port>		groups=FD,SOCKET,IP4,UDP
       UDPLITE6-CONNECT:<host>:<port>		groups=FD,SOCKET,IP6,UDP
       UDPLITE6-DATAGRAM:<host>:<port>		groups=FD,SOCKET,RANGE,IP6,UDP
       UDPLITE6-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RANGE,IP6,UDP
       UDPLITE6-RECV:<port>			groups=FD,SOCKET,RANGE,IP6,UDP
       UDPLITE6-RECVFROM:<port>			groups=FD,SOCKET,CHILD,RANGE,IP6,UDP
       UDPLITE6-SENDTO:<host>:<port>		groups=FD,SOCKET,IP6,UDP
       UNIX-CLIENT:<filename>			groups=FD,SOCKET,NAMED,RETRY,UNIX
       UNIX-CONNECT:<filename>			groups=FD,SOCKET,NAMED,RETRY,UNIX
       UNIX-LISTEN:<filename>			groups=FD,SOCKET,NAMED,LISTEN,CHILD,RETRY,UNIX
       UNIX-RECV:<filename>			groups=FD,SOCKET,NAMED,RETRY,UNIX
       UNIX-RECVFROM:<filename>			groups=FD,SOCKET,NAMED,CHILD,RETRY,UNIX
       UNIX-SENDTO:<filename>			groups=FD,SOCKET,NAMED,RETRY,UNIX
       VSOCK-CONNECT:<cid>:<port>		groups=FD,SOCKET,CHILD,RETRY
       VSOCK-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RETRY
 ```
 
 - - -
 
 ##### socat-broker.sh
 
 
 ```
 root@kali:~# socat-broker.sh -h
 Usage: /usr/bin/socat-broker.sh <options> <listener>
 	<listener> is a passive address like TCP4-L or SSL-L
 	<options>:
 		-d*  -S  -t <timeout>  -T <timeout> 	are passed to socat
 		-V	prints the socat command before starting it
 For example:
 	/usr/bin/socat-broker.sh \
 		TCP4-L:1234
 Then connect with clients to port 1234
 Data sent by any client is forwarded to all other clients
 ```
 
 - - -
 
 ##### socat-chain.sh
 
 
 ```
 root@kali:~# socat-chain.sh -h
 Usage: /usr/bin/socat-chain.sh <options> <address1> <address2> <address3>
 	<address1> is typically a passive (listening) address like
 		TCP-L:1234
 	<address2> must be one of OPENSSL, PROXY, SOCK4, SOCKS4A, or SOCKS5,
 		or SSL-L (passive/listening)
 		Given server hostname and port are ignored and replaced by internal
 		communication point
 	<address3> is typically a client address with protocol like OPENSSL
 	<options>:
 		-d*  -S <sigmask>  -t <timeout>  -T <timeout> 	are passed to socat
 		-V	prints the socat commands before starting them
 Example to drive SOCKS over TLS:
 	/usr/bin/socat-chain.sh \
 		TCP4-L:1234,reuseaddr,fork \
 		SOCKS::<server>:<port> \
 		OPENSSL:10.2.3.4:12345,cafile=...
 	Clients that connect to port 1234 will be forwarded to <server>:<port> using socks
 	over TLS
 ```
 
 - - -
 
 ##### socat-mux.sh
 
 
 ```
 root@kali:~# socat-mux.sh -h
 Usage: /usr/bin/socat-mux.sh <options> <listener> <target>
 Example:
     /usr/bin/socat-mux.sh TCP4-L:1234,reuseaddr,fork TCP:10.2.3.4:12345
 Clients may connect to port 1234; data sent by any client is forwarded to 10.2.3.4,
 data provided by 10.2.3.4 is sent to ALL clients
     <options>:
 	-h	Show this help text and exit
 	-V	Show Socat commands
 	-q	Suppress most messages
 	-d*	Options beginning with -d are passed to Socat processes
 	-l*	Options beginning with -l are passed to Socat processes
 	-b|-S|-t|-T|-l <arg>	These options are passed to Socat processes
 ```
 
 - - -
 
 ##### socat1
 
 Multipurpose relay (SOcket CAT)
 
 ```
 root@kali:~# socat1 -h
 socat by Gerhard Rieger and contributors - see www.dest-unreach.org
 Usage:
 socat [options] <bi-address> <bi-address>
    options (general command line options):
       -V     print version and feature information to stdout, and exit
       -h|-?  print a help text describing command line options and addresses
       -hh    like -h, plus a list of all common address option names
       -hhh   like -hh, plus a list of all available address option names
       -d[ddd]        increase verbosity (use up to 4 times; 2 are recommended)
       -d0|1|2|3|4    set verbosity level (0: Errors; 4 all including Debug)
       -D     analyze file descriptors before loop
       --experimental enable experimental features
       --statistics   output transfer statistics on exit
       -ly[facility]  log to syslog, using facility (default is daemon)
       -lf<logfile>   log to file
       -ls            log to stderr (default if no other log)
       -lm[facility]  mixed log mode (stderr during initialization, then syslog)
       -lp<progname>  set the program name used for logging and vars
       -lu            use microseconds for logging timestamps
       -lh            add hostname to log messages
       -v     verbose text dump of data traffic
       -x     verbose hexadecimal dump of data traffic
       -r <file>      raw dump of data flowing from left to right
       -R <file>      raw dump of data flowing from right to left
       -b<size_t>     set data buffer size (8192)
       -s     sloppy (continue on error)
       -S<sigmask>    log these signals, override default
       -t<timeout>    wait seconds before closing second channel
       -T<timeout>    total inactivity timeout in seconds
       -u     unidirectional mode (left to right)
       -U     unidirectional mode (right to left)
       -g     do not check option groups
       -L <lockfile>  try to obtain lock, or fail
       -W <lockfile>  try to obtain lock, or wait
       -4     prefer IPv4 if version is not explicitly specified
       -6     prefer IPv6 if version is not explicitly specified
    bi-address:  /* is an address that may act both as data sync and source */
       <single-address>
       <single-address>!!<single-address>
    single-address:
       <address-head>[,<opts>]
    address-head:
       ABSTRACT-CLIENT:<filename>		groups=FD,SOCKET,RETRY,UNIX
       ABSTRACT-CONNECT:<filename>		groups=FD,SOCKET,RETRY,UNIX
       ABSTRACT-LISTEN:<filename>		groups=FD,SOCKET,LISTEN,CHILD,RETRY,UNIX
       ABSTRACT-RECV:<filename>			groups=FD,SOCKET,RETRY,UNIX
       ABSTRACT-RECVFROM:<filename>		groups=FD,SOCKET,CHILD,RETRY,UNIX
       ABSTRACT-SENDTO:<filename>		groups=FD,SOCKET,RETRY,UNIX
       ACCEPT-FD:<fdnum>				groups=FD,SOCKET,CHILD,RETRY,RANGE,UNIX,IP4,IP6,UDP,TCP
       CREATE:<filename>				groups=FD,REG,NAMED
       DCCP-CONNECT:<host>:<port>		groups=FD,SOCKET,CHILD,RETRY,IP4,IP6
       DCCP-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RETRY,RANGE,IP4,IP6
       DCCP4-CONNECT:<host>:<port>		groups=FD,SOCKET,CHILD,RETRY,IP4
       DCCP4-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RETRY,RANGE,IP4
       DCCP6-CONNECT:<host>:<port>		groups=FD,SOCKET,CHILD,RETRY,IP6
       DCCP6-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RETRY,RANGE,IP6
       EXEC:<command-line>			groups=FD,FIFO,SOCKET,EXEC,FORK,TERMIOS,PTY,PARENT,UNIX
       FD:<fdnum>				groups=FD,FIFO,CHR,BLK,REG,SOCKET,TERMIOS,UNIX,IP4,IP6,UDP,TCP
       GOPEN:<filename>				groups=FD,FIFO,CHR,BLK,REG,SOCKET,NAMED,OPEN,TERMIOS,UNIX
       INTERFACE:<interface>			groups=FD,SOCKET,INTERFACE
       IP-DATAGRAM:<host>:<protocol>		groups=FD,SOCKET,RANGE,IP4,IP6
       IP-RECV:<protocol>			groups=FD,SOCKET,RANGE,IP4,IP6
       IP-RECVFROM:<protocol>			groups=FD,SOCKET,CHILD,RANGE,IP4,IP6
       IP-SENDTO:<host>:<protocol>		groups=FD,SOCKET,IP4,IP6
       IP4-DATAGRAM:<host>:<protocol>		groups=FD,SOCKET,RANGE,IP4
       IP4-RECV:<protocol>			groups=FD,SOCKET,RANGE,IP4
       IP4-RECVFROM:<protocol>			groups=FD,SOCKET,CHILD,RANGE,IP4
       IP4-SENDTO:<host>:<protocol>		groups=FD,SOCKET,IP4
       IP6-DATAGRAM:<host>:<protocol>		groups=FD,SOCKET,RANGE,IP6
       IP6-RECV:<protocol>			groups=FD,SOCKET,RANGE,IP6
       IP6-RECVFROM:<protocol>			groups=FD,SOCKET,CHILD,RANGE,IP6
       IP6-SENDTO:<host>:<protocol>		groups=FD,SOCKET,IP6
       OPEN:<filename>				groups=FD,FIFO,CHR,BLK,REG,NAMED,OPEN,TERMIOS
       OPENSSL:<host>:<port>			groups=FD,SOCKET,CHILD,RETRY,IP4,IP6,TCP,OPENSSL
       OPENSSL-DTLS-CLIENT:<host>:<port>		groups=FD,SOCKET,CHILD,RETRY,IP4,IP6,UDP,OPENSSL
       OPENSSL-DTLS-SERVER:<port>		groups=FD,SOCKET,LISTEN,CHILD,RETRY,RANGE,IP4,IP6,UDP,OPENSSL
       OPENSSL-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RETRY,RANGE,IP4,IP6,TCP,OPENSSL
       PIPE[:<filename>]				groups=FD,FIFO,NAMED,OPEN
       POSIXMQ-BIDIRECTIONAL:<mqname>		groups=FD,NAMED,RETRY
       POSIXMQ-READ:<mqname>			groups=FD,NAMED,RETRY
       POSIXMQ-RECEIVE:<mqname>			groups=FD,NAMED,CHILD,RETRY
       POSIXMQ-SEND:<mqname>			groups=FD,NAMED,CHILD,RETRY
       PROXY:<proxy-server>:<host>:<port>	groups=FD,SOCKET,CHILD,RETRY,IP4,IP6,TCP,HTTP
       PTY					groups=FD,NAMED,TERMIOS,PTY
       SCTP-CONNECT:<host>:<port>		groups=FD,SOCKET,CHILD,RETRY,IP4,IP6
       SCTP-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RETRY,RANGE,IP4,IP6
       SCTP4-CONNECT:<host>:<port>		groups=FD,SOCKET,CHILD,RETRY,IP4
       SCTP4-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RETRY,RANGE,IP4
       SCTP6-CONNECT:<host>:<port>		groups=FD,SOCKET,CHILD,RETRY,IP6
       SCTP6-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RETRY,RANGE,IP6
       SHELL:<shell-command>			groups=FD,FIFO,SOCKET,EXEC,FORK,SHELL,TERMIOS,PTY,PARENT,UNIX
       SOCKET-CONNECT:<domain>:<protocol>:<remote-address>	groups=FD,SOCKET,CHILD,RETRY
       SOCKET-DATAGRAM:<domain>:<type>:<protocol>:<remote-address>	groups=FD,SOCKET,RANGE
       SOCKET-LISTEN:<domain>:<protocol>:<local-address>	groups=FD,SOCKET,LISTEN,CHILD,RETRY,RANGE
       SOCKET-RECV:<domain>:<type>:<protocol>:<local-address>	groups=FD,SOCKET,RANGE
       SOCKET-RECVFROM:<domain>:<type>:<protocol>:<local-address>	groups=FD,SOCKET,CHILD,RANGE
       SOCKET-SENDTO:<domain>:<type>:<protocol>:<remote-address>	groups=FD,SOCKET
       SOCKETPAIR:<filename>			groups=FD,SOCKET
       SOCKS4:<socks-server>:<host>:<port>	groups=FD,SOCKET,CHILD,RETRY,IP4,IP6,TCP,SOCKS4
       SOCKS4A:<socks-server>:<host>:<port>	groups=FD,SOCKET,CHILD,RETRY,IP4,IP6,TCP,SOCKS4
       SOCKS5-CONNECT:<socks-server>:<socks-port>:<target-host>:<target-port>	groups=FD,SOCKET,CHILD,RETRY,IP4,IP6,TCP
       SOCKS5-LISTEN:<socks-server>:<socks-port>:<listen-host>:<listen-port>	groups=FD,SOCKET,CHILD,RETRY,IP4,IP6,TCP
       STDERR					groups=FD,FIFO,CHR,BLK,REG,SOCKET,TERMIOS,UNIX,IP4,IP6,UDP,TCP
       STDIN					groups=FD,FIFO,CHR,BLK,REG,SOCKET,TERMIOS,UNIX,IP4,IP6,UDP,TCP
       STDIO					groups=FD,FIFO,CHR,BLK,REG,SOCKET,TERMIOS,UNIX,IP4,IP6,UDP,TCP
       STDOUT					groups=FD,FIFO,CHR,BLK,REG,SOCKET,TERMIOS,UNIX,IP4,IP6,UDP,TCP
       SYSTEM:<shell-command>			groups=FD,FIFO,SOCKET,EXEC,FORK,TERMIOS,PTY,PARENT,UNIX
       TCP-CONNECT:<host>:<port>			groups=FD,SOCKET,CHILD,RETRY,IP4,IP6,TCP
       TCP-LISTEN:<port>				groups=FD,SOCKET,LISTEN,CHILD,RETRY,RANGE,IP4,IP6,TCP
       TCP4-CONNECT:<host>:<port>		groups=FD,SOCKET,CHILD,RETRY,IP4,TCP
       TCP4-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RETRY,RANGE,IP4,TCP
       TCP6-CONNECT:<host>:<port>		groups=FD,SOCKET,CHILD,RETRY,IP6,TCP
       TCP6-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RETRY,RANGE,IP6,TCP
       TUN[:<ip-addr>/<bits>]			groups=FD,CHR,OPEN,INTERFACE
       UDP-CONNECT:<host>:<port>			groups=FD,SOCKET,IP4,IP6,UDP
       UDP-DATAGRAM:<host>:<port>		groups=FD,SOCKET,RANGE,IP4,IP6,UDP
       UDP-LISTEN:<port>				groups=FD,SOCKET,LISTEN,CHILD,RANGE,IP4,IP6,UDP
       UDP-RECV:<port>				groups=FD,SOCKET,RANGE,IP4,IP6,UDP
       UDP-RECVFROM:<port>			groups=FD,SOCKET,CHILD,RANGE,IP4,IP6,UDP
       UDP-SENDTO:<host>:<port>			groups=FD,SOCKET,IP4,IP6,UDP
       UDP4-CONNECT:<host>:<port>		groups=FD,SOCKET,IP4,UDP
       UDP4-DATAGRAM:<host>:<port>		groups=FD,SOCKET,RANGE,IP4,UDP
       UDP4-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RANGE,IP4,UDP
       UDP4-RECV:<port>				groups=FD,SOCKET,RANGE,IP4,UDP
       UDP4-RECVFROM:<port>			groups=FD,SOCKET,CHILD,RANGE,IP4,UDP
       UDP4-SENDTO:<host>:<port>			groups=FD,SOCKET,IP4,UDP
       UDP6-CONNECT:<host>:<port>		groups=FD,SOCKET,IP6,UDP
       UDP6-DATAGRAM:<host>:<port>		groups=FD,SOCKET,RANGE,IP6,UDP
       UDP6-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RANGE,IP6,UDP
       UDP6-RECV:<port>				groups=FD,SOCKET,RANGE,IP6,UDP
       UDP6-RECVFROM:<port>			groups=FD,SOCKET,CHILD,RANGE,IP6,UDP
       UDP6-SENDTO:<host>:<port>			groups=FD,SOCKET,IP6,UDP
       UDPLITE-CONNECT:<host>:<port>		groups=FD,SOCKET,IP4,IP6,UDP
       UDPLITE-DATAGRAM:<host>:<port>		groups=FD,SOCKET,RANGE,IP4,IP6,UDP
       UDPLITE-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RANGE,IP4,IP6,UDP
       UDPLITE-RECV:<port>			groups=FD,SOCKET,RANGE,IP4,IP6,UDP
       UDPLITE-RECVFROM:<port>			groups=FD,SOCKET,CHILD,RANGE,IP4,IP6,UDP
       UDPLITE-SENDTO:<host>:<port>		groups=FD,SOCKET,IP4,IP6,UDP
       UDPLITE4-CONNECT:<host>:<port>		groups=FD,SOCKET,IP4,UDP
       UDPLITE4-DATAGRAM:<remote-address>:<port>	groups=FD,SOCKET,RANGE,IP4,UDP
       UDPLITE4-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RANGE,IP4,UDP
       UDPLITE4-RECV:<port>			groups=FD,SOCKET,RANGE,IP4,UDP
       UDPLITE4-RECVFROM:<host>:<port>		groups=FD,SOCKET,CHILD,RANGE,IP4,UDP
       UDPLITE4-SENDTO:<host>:<port>		groups=FD,SOCKET,IP4,UDP
       UDPLITE6-CONNECT:<host>:<port>		groups=FD,SOCKET,IP6,UDP
       UDPLITE6-DATAGRAM:<host>:<port>		groups=FD,SOCKET,RANGE,IP6,UDP
       UDPLITE6-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RANGE,IP6,UDP
       UDPLITE6-RECV:<port>			groups=FD,SOCKET,RANGE,IP6,UDP
       UDPLITE6-RECVFROM:<port>			groups=FD,SOCKET,CHILD,RANGE,IP6,UDP
       UDPLITE6-SENDTO:<host>:<port>		groups=FD,SOCKET,IP6,UDP
       UNIX-CLIENT:<filename>			groups=FD,SOCKET,NAMED,RETRY,UNIX
       UNIX-CONNECT:<filename>			groups=FD,SOCKET,NAMED,RETRY,UNIX
       UNIX-LISTEN:<filename>			groups=FD,SOCKET,NAMED,LISTEN,CHILD,RETRY,UNIX
       UNIX-RECV:<filename>			groups=FD,SOCKET,NAMED,RETRY,UNIX
       UNIX-RECVFROM:<filename>			groups=FD,SOCKET,NAMED,CHILD,RETRY,UNIX
       UNIX-SENDTO:<filename>			groups=FD,SOCKET,NAMED,RETRY,UNIX
       VSOCK-CONNECT:<cid>:<port>		groups=FD,SOCKET,CHILD,RETRY
       VSOCK-LISTEN:<port>			groups=FD,SOCKET,LISTEN,CHILD,RETRY
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
