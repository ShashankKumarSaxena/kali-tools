---
Title: ligolo-ng
Homepage: https://github.com/nicocha30/ligolo-ng
Repository: https://gitlab.com/kalilinux/packages/ligolo-ng
Architectures: amd64 arm64
Version: 0.5.2-0kali2
Metapackages: 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### ligolo-ng
 
  Ligolo-ng is a simple, lightweight and fast tool that allows pentesters to
  establish tunnels from a reverse TCP/TLS connection using a tun interface
  (without the need of SOCKS).
 
 **Installed size:** `16.64 MB`  
 **How to install:** `sudo apt install ligolo-ng`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 {{< /spoiler >}}
 
 ##### ligolo-agent
 
 
 ```
 root@kali:~# ligolo-agent -h
 Usage of ligolo-agent:
   -connect string
     	the target (domain:port)
   -ignore-cert
     	ignore TLS certificate validation (dangerous), only for debug purposes
   -retry
     	auto-retry on error
   -socks string
     	socks5 proxy address (ip:port)
   -socks-pass string
     	socks5 password
   -socks-user string
     	socks5 username
   -v	enable verbose mode
 ```
 
 - - -
 
 ##### ligolo-proxy
 
 
 ```
 root@kali:~# ligolo-proxy -h
 Usage of ligolo-proxy:
   -allow-domains string
     	autocert authorised domains, if empty, allow all domains, multiple domains should be comma-separated.
   -autocert
     	automatically request letsencrypt certificates, requires port 80 to be accessible
   -certfile string
     	TLS server certificate (default "certs/cert.pem")
   -keyfile string
     	TLS server key (default "certs/key.pem")
   -laddr string
     	listening address  (default "0.0.0.0:11601")
   -selfcert
     	dynamically generate self-signed certificates
   -v	enable verbose mode
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
