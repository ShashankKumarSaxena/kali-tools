---
Title: opentaxii
Homepage: https://github.com/eclecticiq/OpenTAXII
Repository: https://gitlab.com/kalilinux/packages/opentaxii
Architectures: all
Version: 0.9.3-0kali2
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### opentaxii
 
  This package contains a robust Python implementation of TAXII Services that
  delivers rich feature set and friendly pythonic API built on top of well
  designed application.
   
  OpenTAXII is guaranteed to be compatible with Cabby, TAXII client library.
 
 **Installed size:** `346 KB`  
 **How to install:** `sudo apt install opentaxii`  
 
 {{< spoiler "Dependencies:" >}}
 * python3
 * python3-blinker
 * python3-flask 
 * python3-jwt
 * python3-libtaxii
 * python3-lxml
 * python3-marshmallow
 * python3-mypy-extensions
 * python3-six 
 * python3-sqlalchemy 
 * python3-stix2
 * python3-structlog
 * python3-tz
 * python3-yaml
 {{< /spoiler >}}
 
 ##### opentaxii-add-api-root
 
 
 ```
 root@kali:~# opentaxii-add-api-root -h
 usage: opentaxii-add-api-root [-h] -t TITLE [-d DESCRIPTION] [--default]
 
 Add a new taxii2 ApiRoot object.
 
 options:
   -h, --help            show this help message and exit
   -t TITLE, --title TITLE
                         Title of the api root (default: None)
   -d DESCRIPTION, --description DESCRIPTION
                         Description of the api root (default: None)
   --default             Set as default api root (default: False)
 ```
 
 - - -
 
 ##### opentaxii-add-collection
 
 
 ```
 root@kali:~# opentaxii-add-collection -h
 usage: opentaxii-add-collection [-h] -r {} -t TITLE [-d DESCRIPTION]
                                 [-a ALIAS] [--public] [--public-write]
 
 Add a new taxii2 Collection object.
 
 options:
   -h, --help            show this help message and exit
   -r {}, --rootid {}    Api root id of the collection (default: None)
   -t TITLE, --title TITLE
                         Title of the collection (default: None)
   -d DESCRIPTION, --description DESCRIPTION
                         Description of the collection (default: None)
   -a ALIAS, --alias ALIAS
                         alias of the collection (default: None)
   --public              allow public read access (default: False)
   --public-write        allow public write access (default: False)
 ```
 
 - - -
 
 ##### opentaxii-create-account
 
 
 ```
 root@kali:~# opentaxii-create-account -h
 usage: opentaxii-create-account [-h] -u USERNAME -p PASSWORD [-a]
 
 Create Account via OpenTAXII Auth API
 
 options:
   -h, --help            show this help message and exit
   -u USERNAME, --username USERNAME
   -p PASSWORD, --password PASSWORD
   -a, --admin           grant admin access (default: False)
 ```
 
 - - -
 
 ##### opentaxii-delete-blocks
 
 
 ```
 root@kali:~# opentaxii-delete-blocks -h
 usage: opentaxii-delete-blocks [-h] -c COLLECTION [-m] --begin BEGIN
                                [--end END]
 
 Delete content blocks from specified collections with timestamp labels
 matching defined time window
 
 options:
   -h, --help            show this help message and exit
   -c COLLECTION, --collection COLLECTION
                         Collection to remove content blocks from (default:
                         None)
   -m, --with-messages   delete inbox messages associated with deleted content
                         blocks (default: False)
   --begin BEGIN         exclusive beginning of time window as ISO8601
                         formatted date (default: None)
   --end END             inclusive ending of time window as ISO8601 formatted
                         date (default: None)
 ```
 
 - - -
 
 ##### opentaxii-job-cleanup
 
 
 ```
 root@kali:~# opentaxii-job-cleanup -h
 No job to remove
 ```
 
 - - -
 
 ##### opentaxii-run-dev
 
 
 ```
 root@kali:~# opentaxii-run-dev -h
  * Serving Flask app 'opentaxii.middleware'
  * Debug mode: on
 ```
 
 - - -
 
 ##### opentaxii-sync-data
 
 
 ```
 root@kali:~# opentaxii-sync-data -h
 usage: opentaxii-sync-data [-h] [-f] config
 
 Create services/collections/accounts
 
 positional arguments:
   config              YAML file with data configuration
 
 options:
   -h, --help          show this help message and exit
   -f, --force-delete  force deletion of collections and their content blocks
                       if collection is not defined in configuration file
                       (default: False)
 ```
 
 - - -
 
 ##### opentaxii-update-account
 
 
 ```
 root@kali:~# opentaxii-update-account -h
 usage: opentaxii-update-account [-h] -u USERNAME -f {password,admin} -v VALUE
 
 Update Account via OpenTAXII Auth API
 
 options:
   -h, --help            show this help message and exit
   -u USERNAME, --username USERNAME
   -f {password,admin}, --field {password,admin}
   -v VALUE, --value VALUE
 ```
 
 - - -
 
 ### opentaxii-doc
 
  This package contains a robust Python implementation of TAXII Services that
  delivers rich feature set and friendly pythonic API built on top of well
  designed application.
   
  OpenTAXII is guaranteed to be compatible with Cabby, TAXII client library.
   
  This is the common documentation package.
 
 **Installed size:** `1.31 MB`  
 **How to install:** `sudo apt install opentaxii-doc`  
 
 {{< spoiler "Dependencies:" >}}
 * libjs-jquery 
 * libjs-sphinxdoc 
 * sphinx-rtd-theme-common 
 {{< /spoiler >}}
 
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
