---
Title: above
Homepage: https://github.com/cursedpkt/Above
Repository: https://gitlab.com/kalilinux/packages/above
Architectures: all
Version: 2.4-0kali1
Metapackages: kali-linux-everything kali-tools-sniffing-spoofing 
Icon: images/above-logo.svg
PackagesInfo: |
 ### above
 
  This package contains an invisible protocol sniffer for finding
  vulnerabilities in the network, designed for pentesters and security
  professionals.
   
  It is based entirely on network traffic analysis, so it does not make any
  noise on the air. Above allows pentesters to automate the process of finding
  vulnerabilities in network hardware. Discovery protocols, dynamic routing,
  FHRP, STP, LLMNR/NBT-NS, etc.
   
  The tool can also both listen to traffic on the interface and analyze already
  existing pcap files.
 
 **Installed size:** `92 KB`  
 **How to install:** `sudo apt install above`  
 
 {{< spoiler "Dependencies:" >}}
 * python3
 * python3-colorama
 * python3-scapy
 {{< /spoiler >}}
 
 ##### above
 
 
 ```
 root@kali:~# above -h
                                         
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   @   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  @@@@@@@@@  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@@@@@   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@@@@@@@   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 @@@@@@@@@@@@@@@@@@@@                                         @@@@@@@@@@@@@@@@@@@
 @@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@@@@@@@@@@@@@@@   @@@@@@@@@@@@@@@@@@@@@@@@@@
 @@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@@@@@@@@@@@@@@@@@@@
 @@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@@@@@@@@@@@@@@@@@@
 @@@@@@@@@@@@@@@@@@@@@@@@  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  @@@@@@@@@@@@@@@@@@@@@@@
 @@@@@@@@@@@@@@@@@@@@@@                                     @@@@@@@@@@@@@@@@@@@@@
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                                         
                        Invisible network protocol sniffer
                  Designed for pentesters and security engineers
               Version 2.4, Codename: My Own Summer (Shove It)
                Author: Magama Bazarov, <caster@exploit.org>
 usage: above [-h] [--interface INTERFACE] [--timer TIMER]
              [--output-pcap OUTPUT_FILE] [--input-pcap INPUT_FILE]
 
 options:
   -h, --help            show this help message and exit
   --interface INTERFACE
                         Specify the interface
   --timer TIMER         Specify the timer value (seconds)
   --output-pcap OUTPUT_FILE
                         Specify the output pcap file to record traffic (hot
                         mode)
   --input-pcap INPUT_FILE
                         Specify the input pcap file to analyze traffic (cold
                         mode)
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
